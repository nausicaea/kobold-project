use serde::{Deserialize, Serialize};
use thiserror::Error;

#[derive(Debug, Clone, Copy, PartialEq, Eq, Serialize, Deserialize)]
pub enum BuildType {
    Release,
    Debug,
}

impl BuildType {
    pub fn to_str(&self) -> &str {
        self.into()
    }
}

impl std::fmt::Display for BuildType {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        write!(f, "{}", Into::<&'static str>::into(self))
    }
}

impl std::str::FromStr for BuildType {
    type Err = Error;

    fn from_str(value: &str) -> Result<BuildType, Self::Err> {
        use self::BuildType::*;
        match value.to_lowercase().as_str() {
            "release" => Ok(Release),
            "debug" => Ok(Debug),
            _ => Err(Error::ParseBuildTypeError(value.to_string())),
        }
    }
}

impl Into<&'static str> for BuildType {
    fn into(self) -> &'static str {
        (&self).into()
    }
}

impl Into<&'static str> for &BuildType {
    fn into(self) -> &'static str {
        use self::BuildType::*;

        match self {
            Release => "Release",
            Debug => "Debug",
        }
    }
}

#[derive(Debug, Error)]
pub enum Error {
    #[error("The value {0} is not understood as a variant of the enum BuildType")]
    ParseBuildTypeError(String),
}
