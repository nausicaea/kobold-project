use crate::android::keystore_credentials::KeystoreCredentials;
use crate::build_target::BuildTarget;
use crate::build_type::BuildType;
use crate::ios::export_destination::ExportDestination;
use crate::ios::export_options::ExportOptions;
use crate::semver::SemVer;
use anyhow::Result;
use directories::UserDirs;
use plist;
use std::fs::rename;
use std::path::{Path, PathBuf};
use std::process::Command;
use tempfile;
use thiserror::Error;
use std::fs;

static SECURITY: &'static str = "/usr/bin/security";
static XCODEBUILD: &'static str = "/usr/bin/xcodebuild";
static LFTP: &'static str = "/usr/local/bin/lftp";
static CHMOD: &'static str = "/bin/chmod";
static CODESIGN: &'static str = "/usr/bin/codesign";
static DITTO: &'static str = "/usr/bin/ditto";
static XCRUN: &'static str = "/usr/bin/xcrun";

pub fn deploy_webgl(
    product_path: &Path,
    sftp_host_path: &str,
    sftp_username: &str,
    sftp_password: &str,
    verbose: u64,
) -> Result<()> {
    let cmd_str = format!(
        "mirror -vvvR -P 10 --loop --skip-noaccess {} ./",
        product_path.display()
    );
    let cred_str = format!("{},{}", sftp_username, sftp_password);

    let prog = LFTP;
    let args = vec!["-e", &cmd_str, "-u", &cred_str, sftp_host_path];
    if verbose > 1 {
        println!("$ {} {}", prog, args.join(" "));
    }

    let _ = Command::new(prog)
        .args(&args)
        .status()
        .map_err(|e| Error::CannotDeployWebGl(e))
        .and_then(|s| {
            if s.success() {
                Ok(s)
            } else {
                Err(Error::NonZeroExitStatus(s))
            }
        })?;

    Ok(())
}

pub fn unlock_keychain(password: &str, verbose: u64) -> Result<()> {
    let prog = SECURITY;
    let args = vec!["unlock-keychain", "-p", password, "login.keychain-db"];
    if verbose > 1 {
        println!("$ {} {}", prog, args.join(" "));
    }

    let _ = Command::new(prog)
        .args(&args)
        .status()
        .map_err(|e| Error::CannotUnlockLoginKeychain(e))
        .and_then(|s| {
            if s.success() {
                Ok(s)
            } else {
                Err(Error::NonZeroExitStatus(s))
            }
        })?;

    Ok(())
}

pub fn build_ios_xcarchive(
    xcodeproj_path: &Path,
    xcarchive_path: &Path,
    build_type: BuildType,
    scheme: &str,
    keychain_password: &str,
    verbose: u64,
) -> Result<()> {
    // Clean the derived data archive
    if let Some(user_dirs) = UserDirs::new() {
        let derived_data = user_dirs.home_dir().join("Developer/Xcode/DerivedData");
        if derived_data.exists() {
            std::fs::remove_dir_all(&derived_data)
                .map_err(|e| Error::CannotRemoveDerivedData(derived_data, e))?;
        }
    }

    // Unlock the login keychain
    unlock_keychain(keychain_password, verbose)?;

    // Build the xcarchive
    let xcap = xcarchive_path.with_file_name(xcarchive_path.file_stem().unwrap());
    let xcproj_str = format!("{}", xcodeproj_path.display());
    let xcap_str = format!("{}", xcap.display());
    let bt_str = format!("{}", build_type);
    let prog = XCODEBUILD;
    let args = vec![
        "-project",
        &xcproj_str,
        "-scheme",
        &scheme,
        "-sdk",
        "iphoneos",
        "-configuration",
        &bt_str,
        "-allowProvisioningUpdates",
        "archive",
        "-archivePath",
        &xcap_str,
    ];

    if verbose > 1 {
        println!("$ {} {}", prog, args.join(" "));
    }

    let _ = Command::new(prog)
        .args(&args)
        .status()
        .map_err(|e| Error::CannotBuildXcarchive(e))
        .and_then(|s| {
            if s.success() {
                Ok(s)
            } else {
                Err(Error::NonZeroExitStatus(s))
            }
        })?;

    Ok(())
}

pub fn export_ios_ipa(
    destination: ExportDestination,
    xcarchive_path: &Path,
    export_path: &Path,
    dev_team: &str,
    keychain_password: &str,
    verbose: u64,
) -> Result<()> {
    // Unlock the login keychain
    unlock_keychain(keychain_password, verbose)?;

    // Create the export options plist file
    let dir = tempfile::tempdir().map_err(|e| Error::CannotCreateTempDir(e))?;
    let file_path = dir.path().join("export-options.plist");
    let exopt = ExportOptions::new(dev_team, destination);
    plist::to_file_xml(&file_path, &exopt).map_err(|e| Error::CannotSerializeExportOptions(e))?;
    if verbose > 1 {
        println!(
            "Generated the export options\n{}",
            std::fs::read_to_string(&file_path).unwrap()
        )
    }

    // Export the ipa
    let xcap_str = format!("{}", xcarchive_path.display());
    let exoptp_str = format!("{}", file_path.display());
    let exp_str = format!("{}", export_path.display());
    let prog = XCODEBUILD;
    let args = vec![
        "-allowProvisioningUpdates",
        "-exportArchive",
        "-archivePath",
        &xcap_str,
        "-exportOptionsPlist",
        &exoptp_str,
        "-exportPath",
        &exp_str,
    ];

    if verbose > 1 {
        println!("$ {} {}", prog, args.join(" "));
    }

    let _ = Command::new(prog)
        .args(&args)
        .status()
        .map_err(|e| Error::CannotBuildIpa(e))
        .and_then(|s| {
            if s.success() {
                Ok(s)
            } else {
                Err(Error::NonZeroExitStatus(s))
            }
        })?;

    Ok(())
}

pub fn build_unity(
    unity_path: &Path,
    cache_server: &str,
    project_path: &Path,
    method: &str,
    build_type: BuildType,
    target: BuildTarget,
    build_number: usize,
    build_path: &Path,
    developer_team: Option<&str>,
    keystore: Option<&KeystoreCredentials>,
    strict: bool,
    verbose: u64,
) -> Result<()> {
    let project_path_str = format!("{}", project_path.display());
    let bn_str = format!("{}", build_number);
    let bt_str = format!("{}", build_type);
    let op_str = format!("{}", build_path.display());
    let target_str = format!("{}", target);

    let mut args = vec![
        "-batchmode",
        "-quit",
        "-logFile",
        "-",
        "-CacheServerIPAddress",
        cache_server,
        "-projectPath",
        &project_path_str,
        "-buildTarget",
        &target_str,
        "-executeMethod",
        method,
        "--",
        "-buildNumber",
        &bn_str,
        "-buildType",
        &bt_str,
        "-outputPath",
        &op_str,
    ];

    if let Some(ref d) = developer_team {
        args.append(&mut vec!["-developerTeamId", d]);
    }

    if let Some(k) = keystore {
        args.append(&mut vec![
            "-keystorePath",
            &k.path,
            "-keystorePassword",
            &k.password,
            "-keyaliasName",
            &k.alias,
            "-keyaliasPassword",
            &k.alias_password,
        ]);
    }

    if strict {
        args.push("-strict");
    }

    // This line is for backwards compatibility only
    args.push(&target_str);

    if verbose > 1 {
        println!("{} {}", unity_path.display(), args.join(" "));
    }

    let _ = Command::new(unity_path)
        .args(&args)
        .status()
        .map_err(|e| Error::CannotBuildUnityProject(e))
        .and_then(|s| {
            if s.success() {
                Ok(s)
            } else {
                Err(Error::NonZeroExitStatus(s))
            }
        })?;

    Ok(())
}

pub fn archive_android(
    version: &SemVer,
    target: BuildTarget,
    product_path: &Path,
) -> Result<()> {
    let archive_destination = archive_path(product_path, version, target, "aab");

    let _ = rename(&product_path, &archive_destination)
        .map_err(|e| Error::CannotArchiveApk(product_path.to_path_buf(), archive_destination, e))?;

    Ok(())
}

pub fn archive_ios(
    version: &SemVer,
    target: BuildTarget,
    export_path: &Path,
) -> Result<()> {
    let archive_destination = archive_path(export_path, version, target, "ipa");

    let _ = rename(&export_path, &archive_destination)
        .map_err(|e| Error::CannotArchiveIpa(export_path.to_path_buf(), archive_destination, e))?;

    Ok(())
}

pub fn archive_webgl(product_path: &Path) -> Result<()> {
    // Determine if the index file at the target actually exists.
    //let _ = BuildTarget::WebGl
    //    .archive_source(&product_path)
    //    .map(|p| p.join("index.html"))
    //    .filter(|p| p.is_file())
    //    .ok_or(Error::CannotFindIndex(product_path.to_path_buf()))?;

    archive_other(BuildTarget::WebGl, product_path)
}

pub fn archive_other(target: BuildTarget, product_path: &Path) -> Result<()> {
    let archive_source = target.archive_source(&product_path).filter(|p| p.is_dir());

    if let Some(archive_source) = archive_source {
        let working_dir = archive_source
            .parent()
            .expect("The archive source has no parent directory");

        let archive_folder = archive_source
            .file_name()
            .and_then(|f| f.to_str())
            .expect("Cannot determine the archive source folder name");

        let mut product_path_actual = product_path;
        let mut product_path_parent = product_path.parent().unwrap();
        while !product_path_parent.ends_with("build") {
            product_path_actual = product_path_parent;
            product_path_parent = product_path_parent.parent().unwrap();
        }

        let archive_name = format!("{}.zip", product_path_actual.file_name().unwrap().to_str().unwrap());

        let _ = Command::new("zip")
            .args(&["-r", &archive_name, archive_folder])
            .current_dir(working_dir)
            .status()
            .map_err(|e| Error::CannotArchiveFolder(e))
            .and_then(|s| {
                if s.success() {
                    Ok(s)
                } else {
                    Err(Error::NonZeroExitStatus(s))
                }
            })?;

        Ok(())
    } else {
        eprintln!("Warning: Cannot produce an archive for the specified target and product, because the product is already an archive");
        Ok(())
    }
}

/// Create a PlasticSCM label to mark a particular build
pub fn create_label_plasticscm(
    scm_root: &Path,
    app_name: &str,
    version: &SemVer,
    target: BuildTarget,
) -> Result<()> {
    let label_name = format!(
        "lb:{} {} {}",
        app_name,
        target.to_str().to_lowercase(),
        version.internal()
    );
    let _ = Command::new("/usr/local/bin/cm")
        .args(&["label", "create", &label_name])
        .current_dir(scm_root)
        .status()
        .map_err(|e| Error::CannotLabelChangeset(e))
        .and_then(|s| {
            if s.success() {
                Ok(s)
            } else {
                Err(Error::NonZeroExitStatus(s))
            }
        })?;

    Ok(())
}

/// Given the path to a unity editor binary, determine its version
pub fn find_unity_version(unity_editor: &Path) -> Result<String> {
    if !unity_editor.is_file() {
        return Err(Error::NotAFile(unity_editor.to_path_buf()).into());
    }

    let ep_str = format!("{}", unity_editor.display());
    let output = Command::new("grep")
        .args(&[
            "--binary-files=text",
            "-o",
            r#"Unity\s\d\+\.\d\+\.\d\+f\d\+"#,
            &ep_str,
        ])
        .output()
        .map_err(|e| Into::<anyhow::Error>::into(e))
        .and_then(|o| String::from_utf8(o.stdout).map_err(|e| e.into()))
        .map(|s| s.trim().to_string())?;

    Ok(output)
}

pub fn sign_osx(product_path: &Path, signing_id: &str) -> Result<()> {
    // fix rights
    let chmod_status = Command::new(CHMOD)
        .args(&[
            "-R",
            "a+xr",
            product_path.to_str().unwrap(),
        ])
        .status()
        .map_err(|e| Error::CannotChmod(product_path.to_path_buf(),e));
    if chmod_status.is_ok() {
        println!("Fixed access rights in {}", product_path.display());
    } else {
        return Err(anyhow::Error::from(chmod_status.unwrap_err()));
    }

    //sign everything
    let mut entitlements_path = PathBuf::from(product_path);
    entitlements_path.set_extension("entitlements");
    let codesign_status = Command::new(CODESIGN)
        .args(&[
            "--deep",
            "--force",
            "--verify",
            "--verbose",
            "--timestamp",
            "--options",
            "runtime",
            "--entitlements",
            entitlements_path.to_str().unwrap(),
            "--sign",
            signing_id,
            product_path.to_str().unwrap()
        ])
        .status()
        .map_err(|e| Error::CannotSign(product_path.to_path_buf(),e));
    if codesign_status.is_err() {
        return Err(anyhow::Error::from(codesign_status.unwrap_err()));
    }

    Ok(())
}

fn archive_path(product_path: &Path, version: &SemVer, target: BuildTarget, extension: &str) -> PathBuf {
    product_path.with_file_name(format!(
        "{}_{}_v{}.{}",
        product_path.file_stem().unwrap().to_str().unwrap(),
        target.to_short_str().to_lowercase(),
        version.public(),
        extension
    ))
}

pub fn archive_other2(product_path: &Path, version: &SemVer, target: BuildTarget) -> Result<()> {
    let archive_destination = archive_path(product_path, version, target, "zip");
    let archive_folder = archive_destination.with_extension("");

    // create archive_folder
    if archive_folder.exists() {
        Command::new("rm")
         .arg("-r")
         .arg(&archive_folder)
         .output()?;
    }
    fs::create_dir(&archive_folder)?;

    // copy files
    for entry in fs::read_dir(product_path.parent().unwrap())? {
       let entry = entry?;
       let path = entry.path();
       if !path.to_str().unwrap().eq(archive_folder.to_str().unwrap()) && !path.to_str().unwrap().contains("DoNotShip") {
           let dst_name = path.to_str().unwrap().rsplit_once(std::path::MAIN_SEPARATOR).unwrap();
           let dst = archive_folder.join(dst_name.1);

           if path.is_dir() {
               Command::new("cp")
                .arg("-r")
                .arg(path)
                .arg(dst)
                .output()?;
            } else {
                fs::copy(path, dst)?;
            }
       }
    }

    // zip
    let ditto_status = Command::new(DITTO)
        .args(&[
            "-c",
            "-k",
            "--sequesterRsrc",
            "--keepParent",
            archive_folder.to_str().unwrap(),
            archive_destination.to_str().unwrap()
        ])
        .status()
        .map_err(|e| Error::CannotDitto(archive_folder.to_path_buf(),e));
    if ditto_status.is_err() {
        return Err(anyhow::Error::from(ditto_status.unwrap_err()));
    }

    // delete archive_folder
    Command::new("rm")
     .arg("-r")
     .arg(archive_folder)
     .output()?;

    Ok(())
}

pub fn archive_osx(product_path: &Path, version: &SemVer, target: BuildTarget) -> Result<()> {
    //package
    let archive_destination = archive_path(product_path, version, target, "zip");

    let ditto_status = Command::new(DITTO)
        .args(&[
            "-c",
            "-k",
            "--sequesterRsrc",
            "--keepParent",
            product_path.to_str().unwrap(),
            archive_destination.to_str().unwrap()
        ])
        .status()
        .map_err(|e| Error::CannotDitto(product_path.to_path_buf(),e));
    if ditto_status.is_err() {
        return Err(anyhow::Error::from(ditto_status.unwrap_err()));
    }

    Ok(())
}

pub fn notarize_mac_app(product_path: &Path, apple_id: &str, password: &str, short_provider_name: &str, version: &SemVer, target: BuildTarget) -> Result<()> {
    let archive_path = archive_path(product_path, version, target, "zip");

    //upload to apple
    let xcrun_status = Command::new(XCRUN)
        .args(&[
            "notarytool",
            "submit",
            "--apple-id",
            apple_id,
            "--password",
            password,
            "--team-id",
            short_provider_name,
            archive_path.to_str().unwrap()
        ])
        .status()
        .map_err(|e| Error::CannotXCRunAltoolNotarize(archive_path.to_path_buf(),e));
    if xcrun_status.is_err() {
        return Err(anyhow::Error::from(xcrun_status.unwrap_err()));
    }

    Ok(())
}

#[derive(Debug, Error)]
pub enum Error {
    #[error("The path {} does not point to a file", .0.display())]
    NotAFile(PathBuf),
    #[error("Cannot deploy the WebGL build")]
    CannotDeployWebGl(#[source] std::io::Error),
    #[error("Cannot validate the iOS IPA via altool")]
    CannotValidateIpaAltool(#[source] std::io::Error),
    #[error("Cannot upload the iOS IPA via altool")]
    CannotUploadIpaAltool(#[source] std::io::Error),
    #[error("Cannot unlock the login keychain")]
    CannotUnlockLoginKeychain(#[source] std::io::Error),
    #[error("Cannot build the xcarchive")]
    CannotBuildXcarchive(#[source] std::io::Error),
    #[error("Cannot create a temporary directory")]
    CannotCreateTempDir(#[source] std::io::Error),
    #[error("Cannot serialize the ExportOptions to a plist file")]
    CannotSerializeExportOptions(#[source] plist::Error),
    #[error("Cannot build the iOS IPA")]
    CannotBuildIpa(#[source] std::io::Error),
    #[error("Cannot build the unity project")]
    CannotBuildUnityProject(#[source] std::io::Error),
    #[error("Cannot move the Android APK from {} to {}", .0.display(), .1.display())]
    CannotArchiveApk(PathBuf, PathBuf, #[source] std::io::Error),
    #[error("Cannot move the iOS IPA from {} to {}", .0.display(), .1.display())]
    CannotArchiveIpa(PathBuf, PathBuf, #[source] std::io::Error),
    #[error("Cannot create a zip archive of the product")]
    CannotArchiveFolder(#[source] std::io::Error),
    #[error("The index.html file does not exist at {}", .0.display())]
    CannotFindIndex(PathBuf),
    #[error("Cannot label the changeset")]
    CannotLabelChangeset(#[source] std::io::Error),
    #[error("Cannot determine the version of the unity editor")]
    CannotFindUnityVersion(#[source] std::io::Error),
    #[error("The process failed with a non-zero exit status {0}")]
    NonZeroExitStatus(std::process::ExitStatus),
    #[error("Cannot remove the derived data archive of XCode at {}", .0.display())]
    CannotRemoveDerivedData(PathBuf, #[source] std::io::Error),
    #[error("Cannot fix rights within {}", .0.display())]
    CannotChmod(PathBuf, #[source] std::io::Error),
    #[error("Cannot deep sign {}", .0.display())]
    CannotSign(PathBuf, #[source] std::io::Error),
    #[error("Cannot zip {}", .0.display())]
    CannotDitto(PathBuf, #[source] std::io::Error),
    #[error("Cannot xcrun altool --notarize-app {}", .0.display())]
    CannotXCRunAltoolNotarize(PathBuf, #[source] std::io::Error),
    #[error("Cannot move the OSX APP from {} to {}", .0.display(), .1.display())]
    CannotArchiveApp(PathBuf, PathBuf, #[source] std::io::Error),
}
