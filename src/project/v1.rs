//! This module contains version 1 of the project configuration specification.
use crate::semver::SemVer;
use serde::{Deserialize, Serialize};

/// A `Project` defines settings that relate to a single Unity project.
#[derive(Debug, Serialize, Deserialize)]
#[serde(rename_all = "PascalCase")]
pub struct Project {
    /// Each project may have an internal name, not shown publicly.
    pub name: String,
    /// Each project will itself have a version. Here, we follow the [SemVer](https://semver.org/)
    /// convention with a few minor adjustments. The public representation of the version is
    /// assigned to `PlayerSettings.bundleVersion`. An iOS-specific representation is assigned to
    /// `PlayerSettings.iOS.buildNumber`, and an Android-specific representation is assigned to
    /// `PlayerSettings.Android.bundleVersionCode`.
    #[serde(default)]
    pub version: SemVer,
    /// Defines the scenes that will be included in the build. This must be a list of paths
    /// relative to the Unity project directory (ex. `/Assets/0_scenes/main.unity`).
    #[serde(default)]
    pub scenes: Vec<String>,
}
