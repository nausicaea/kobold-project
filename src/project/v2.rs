//! This module contains version 2 of the project configuration specification.
use crate::semver::SemVer;
use serde::{Deserialize, Serialize};

/// A `Product` defines all settings necessary for Unity to produce an application (ex. an EXE
/// binary, an APK or a WebGL build).
#[derive(Debug, Serialize, Deserialize)]
#[serde(rename_all = "PascalCase")]
pub struct Product {
    /// Defines the application name (assigned to `PlayerSettings.productName`).
    pub identifier: String,
    /// Defines the scenes that will be included in the build. This must be a list of paths
    /// relative to the Unity project directory (ex. `/Assets/0_scenes/main.unity`).
    #[serde(default)]
    pub scenes: Vec<String>,
    /// Defines the files and folders that are allowed to reside in `StreamingAssets` in the final
    /// application. Any files and folders not listed here will be deleted from the build. This
    /// will never modify the contents of `/Assets/StreamingAssets` in the Unity project.
    /// Furthermore, this only works for builds that produce a directory (Standalone Windows, macOS
    /// and Linux, as well as WebGL builds).
    #[serde(default)]
    pub streaming_assets_whitelist: Vec<String>,
}

/// A `Project` defines settings that relate to a single Unity project.
#[derive(Debug, Serialize, Deserialize)]
#[serde(rename_all = "PascalCase")]
pub struct Project {
    /// Each project may have an internal name, not shown publicly.
    pub name: String,
    /// Each project will itself have a version. Here, we follow the [SemVer](https://semver.org/)
    /// convention with a few minor adjustments. The public representation of the version is
    /// assigned to `PlayerSettings.bundleVersion`. An iOS-specific representation is assigned to
    /// `PlayerSettings.iOS.buildNumber`, and an Android-specific representation is assigned to
    /// `PlayerSettings.Android.bundleVersionCode`.
    #[serde(default)]
    pub version: SemVer,
    /// Each project may define one or more build products or applications.
    #[serde(default)]
    pub products: Vec<Product>,
}
