//! This module contains version 6 of the project configuration specification.
use crate::build_target::BuildTarget;
use crate::semver::SemVer;
use serde::{Deserialize, Serialize};

/// Defines a splash screen logo.
#[derive(Debug, Serialize, Deserialize)]
#[serde(rename_all = "PascalCase")]
pub struct SplashLogo {
    /// The path to the splash screen logo.
    pub sprite_path: String,
    /// The duration of the splash screen.
    pub duration: f32,
}

#[derive(Debug, Serialize, Deserialize)]
pub struct Color {
    pub r: f32,
    pub g: f32,
    pub b: f32,
    pub a: f32,
}

#[derive(Debug, Serialize, Deserialize)]
#[serde(rename_all = "PascalCase")]
pub enum DrawMode {
    UnityLogoBelow,
    AllSequential,
}

#[derive(Debug, Serialize, Deserialize)]
#[serde(rename_all = "PascalCase")]
pub enum UnityLogoStyle {
    DarkOnLight,
    LightOnDark,
}

#[derive(Debug, Serialize, Deserialize)]
#[serde(rename_all = "PascalCase")]
pub enum AnimationMode {
    Static,
    Dolly,
    Custom,
}

/// `SplashDefinition` contains settings for the Unity game splash screen (at the beginning of a
/// game).
#[derive(Debug, Serialize, Deserialize)]
pub struct SplashDefinition {
    #[serde(rename = "SplashLogos")]
    #[serde(default)]
    pub splash_logos: Vec<SplashLogo>,
    #[serde(rename = "Show")]
    pub show: bool,
    #[serde(rename = "ShowUnity")]
    pub show_unity: bool,
    #[serde(rename = "DrawMode")]
    pub draw_mode: DrawMode,
    #[serde(rename = "LogoStyle")]
    pub logo_style: UnityLogoStyle,
    #[serde(rename = "AnimationMode")]
    pub animation_mode: AnimationMode,
    #[serde(rename = "Zoom")]
    pub zoom: f32,
    #[serde(rename = "BGZoom")]
    pub bg_zoom: f32,
    #[serde(rename = "OverlayOpacity")]
    pub overlay_opacity: f32,
    #[serde(rename = "BackgroundColor")]
    pub background_color: Color,
    #[serde(rename = "BackgroundPath")]
    pub background_path: String,
}

#[derive(Debug, Serialize, Deserialize)]
#[serde(rename_all = "PascalCase")]
pub enum BuildSteps {
    FullPipeline,
    BuildOnly,
    BuildAndVerify,
}

/// Specifies a build target / platform.
#[derive(Debug, Serialize, Deserialize)]
#[serde(rename_all = "PascalCase")]
pub struct BuildTargetSpec {
    pub identifier: String,
    /// Defines the internal identifier for the product (used to separate different products).
    pub target: BuildTarget,
    pub build_steps: BuildSteps,
}

/// A `Product` defines all settings necessary for Unity to produce an application (ex. an EXE
/// binary, an APK or a WebGL build).
#[derive(Debug, Serialize, Deserialize)]
#[serde(rename_all = "PascalCase")]
pub struct Product {
    /// Defines the application name (assigned to `PlayerSettings.productName`).
    pub title: String,
    /// Defines the application identifier (assigned to `PlayerSettings.applicationIdentifier`).
    pub internal_identifier: String,
    /// Defines the scenes that will be included in the build. This must be a list of paths
    /// relative to the Unity project directory (ex. `/Assets/0_scenes/main.unity`).
    #[serde(default)]
    pub scenes: Vec<String>,
    /// Defines the files and folders that are allowed to reside in `StreamingAssets` in the final
    /// application. Any files and folders not listed here will be deleted from the build. This
    /// will never modify the contents of `/Assets/StreamingAssets` in the Unity project.
    /// Furthermore, this only works for builds that produce a directory (Standalone Windows, macOS
    /// and Linux, as well as WebGL builds).
    #[serde(default)]
    pub streaming_assets_whitelist: Vec<String>,
    /// Defines settings for the Unity game splash screen.
    pub splash_definition: SplashDefinition,
    /// Defines the enabled build platforms / targets for the specified product.
    #[serde(default)]
    pub build_targets: Vec<BuildTargetSpec>,
}

/// A `Project` defines settings that relate to a single Unity project.
#[derive(Debug, Serialize, Deserialize)]
#[serde(rename_all = "PascalCase")]
pub struct Project {
    /// Each project may have an internal name, not shown publicly.
    pub name: String,
    /// Each project will be built with a particular Unity version. When builds are made, this
    /// variable will be set to the appropriate Unity editor version.
    #[serde(default)]
    pub unity_version: String,
    /// Each project will itself have a version. Here, we follow the [SemVer](https://semver.org/)
    /// convention with a few minor adjustments. The public representation of the version is
    /// assigned to `PlayerSettings.bundleVersion`. An iOS-specific representation is assigned to
    /// `PlayerSettings.iOS.buildNumber`, and an Android-specific representation is assigned to
    /// `PlayerSettings.Android.bundleVersionCode`.
    #[serde(default)]
    pub version: SemVer,
    /// Each project may define one or more build products or applications.
    #[serde(default)]
    pub products: Vec<Product>,
    /// Each product, if listed in the build queue, will be built.
    #[serde(default)]
    pub build_queue: Vec<String>,
}

impl Project {
    /// Returns the shortened unity version
    pub fn unity_version_short(&self) -> String {
        let parts = self.unity_version.split('.').take(2).collect::<Vec<_>>();

        format!("{}.{}", parts[0], parts[1])
    }

    pub fn unity_major_version(&self) -> usize {
        self.unity_version.split('.').take(1).last().and_then(|v| v.parse::<usize>().ok()).unwrap()  // Option<str> --> Option<usize> --> usize oder panic
    }

    pub fn unity_minor_version(&self) -> usize {
        self.unity_version.split('.').take(2).last().and_then(|v| v.parse::<usize>().ok()).unwrap()  // Option<str> --> Option<usize> --> usize oder panic
    }
}
