use super::export_destination::ExportDestination;
use super::export_method::ExportMethod;
use serde::{Deserialize, Serialize};

#[derive(Debug, Clone, PartialEq, Eq, Serialize, Deserialize)]
pub struct ExportOptions {
    destination: ExportDestination,
    method: ExportMethod,
    #[serde(rename = "teamID")]
    team_id: String,
}

impl ExportOptions {
    pub fn new(team_id: &str, destination: ExportDestination) -> Self {
        let method = match destination {
            ExportDestination::Export => ExportMethod::Development,
            ExportDestination::Upload => ExportMethod::AppStore,
        };

        ExportOptions {
            destination,
            method,
            team_id: team_id.into(),
        }
    }
}
