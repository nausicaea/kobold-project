//! The module `semver` defines the structure `SemVer`, a representation of the semantic versioning
//! convention.

use serde::{Deserialize, Serialize};

fn default_build_metadata() -> String {
    "0".into()
}

/// The struct `SemVer` represents the semantic versioning convention (see
/// [semver.org](https://semver.org/)).
#[derive(Debug, Clone, Serialize, Deserialize, PartialEq, Eq, Hash)]
#[serde(rename_all = "PascalCase")]
pub struct SemVer {
    /// Determines the major version number of a project.
    #[serde(default)]
    pub major: usize,
    /// Determines the minor version number of a project.
    #[serde(default)]
    pub minor: usize,
    /// Determines the patch version number of a project.
    #[serde(default)]
    pub patch: usize,
    /// Determines prerelease information.
    #[serde(default = "String::new")]
    pub prerelease: String,
    /// Determines build metadata.
    #[serde(default = "default_build_metadata")]
    pub build_metadata: String,
}

impl SemVer {
    /// Returns a public representation of the version.
    pub fn public(&self) -> String {
        format!("{}.{}.{}", self.major, self.minor, self.patch)
    }

    /// Returns an internal representation of the version that includes build metadata.
    pub fn internal(&self) -> String {
        format!(
            "v{}.{}.{}b{}",
            self.major, self.minor, self.patch, self.build_metadata
        )
    }

    /// Returns an iOS-compatible representation of the version.
    pub fn ios(&self) -> Option<String> {
        self.build_metadata
            .parse::<usize>()
            .map(|b| format!("{}.{}.{}.{}", self.major, self.minor, self.patch, b))
            .ok()
    }

    /// Returns an Android-compatible representation of the version.
    pub fn android(&self) -> Option<usize> {
        self.ios().and_then(|v| v.replace(".", "").parse().ok())
    }

    /// Returns the canonical representation of the version according to
    /// [SemVer](https://semver.org/).
    pub fn canonical(&self) -> String {
        let mut output = String::new();

        if self.prerelease.len() > 0 {
            output.push('-');
            output.push_str(&self.prerelease);
        }

        if self.build_metadata.len() > 0 {
            output.push('+');
            output.push_str(&self.build_metadata);
        }

        format!("{}.{}.{}{}", self.major, self.minor, self.patch, output)
    }
}

impl Default for SemVer {
    fn default() -> Self {
        SemVer {
            major: 0,
            minor: 0,
            patch: 0,
            prerelease: String::new(),
            build_metadata: default_build_metadata(),
        }
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn default() {
        let s = SemVer::default();
        assert_eq!(
            s,
            SemVer {
                major: 0,
                minor: 0,
                patch: 0,
                prerelease: String::new(),
                build_metadata: "0".into()
            }
        );
    }

    #[test]
    fn public() {
        assert_eq!(SemVer::default().public(), String::from("0.0.0"));
    }

    #[test]
    fn internal() {
        assert_eq!(SemVer::default().internal(), String::from("v0.0.0b0"));
    }

    #[test]
    fn ios() {
        assert_eq!(SemVer::default().ios(), Some(String::from("0.0.0.0")));
    }

    #[test]
    fn android() {
        assert_eq!(SemVer::default().android(), Some(0));
    }

    #[test]
    fn canonical() {
        assert_eq!(SemVer::default().canonical(), String::from("0.0.0+0"));
    }
}
