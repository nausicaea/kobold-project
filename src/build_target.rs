use serde::de::{self, Deserialize, Deserializer, Visitor};
use serde::ser::{Serialize, Serializer};
use std::path::{Path, PathBuf};
use std::str::FromStr;
use thiserror::Error;

#[derive(Debug, Clone, Copy, PartialEq, Eq)]
pub enum BuildTarget {
    Android,
    Ios,
    N3ds,
    Ps4,
    Psp2,
    StandaloneLinux,
    StandaloneLinux64,
    StandaloneLinuxUniversal,
    StandaloneOSX,
    StandaloneOSXUniversal,
    StandaloneWindows,
    StandaloneWindows64,
    Switch,
    Tvos,
    WebGl,
    WsaPlayer,
    XboxOne,
}

impl BuildTarget {
    pub fn to_str(&self) -> &str {
        self.into()
    }

    /// Returns the product filename extension in dependence of the `BuildTarget`.
    pub fn extension(&self) -> Option<&'static str> {
        use self::BuildTarget::*;

        match self {
            StandaloneWindows | StandaloneWindows64 => Some("exe"),
            StandaloneOSX => Some("app"),
            StandaloneOSXUniversal => Some("app"),
            Android => Some("aab"),
            _ => None,
        }
    }

    pub fn to_short_str(&self) -> &str {
        use self::BuildTarget::*;
        
        match self {
            Android => "android",
            Ios => "ios",
            N3ds => "3ds",
            Ps4 => "ps4",
            Psp2 => "ps2",
            StandaloneLinux | StandaloneLinux64 | StandaloneLinuxUniversal => "lin",
            StandaloneOSX | StandaloneOSXUniversal => "osx",
            StandaloneWindows | StandaloneWindows64 => "win",
            Switch => "swch",
            Tvos => "tvos",
            WebGl => "web",
            WsaPlayer => "wsa",
            XboxOne => "xbox"
        }
    }

    pub fn archive_source<P: AsRef<Path>>(&self, path: P) -> Option<PathBuf> {
        use self::BuildTarget::*;

        let tmp = match self {
            StandaloneWindows
            | StandaloneWindows64
            | StandaloneLinux
            | StandaloneLinux64
            | StandaloneLinuxUniversal => path.as_ref().parent(),
            WebGl => Some(path.as_ref()),
            StandaloneOSX
            | StandaloneOSXUniversal => Some(path.as_ref()),
            _ => None,
        };

        tmp.map(|p| p.to_path_buf())
    }
}

impl std::fmt::Display for BuildTarget {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        write!(f, "{}", Into::<&'static str>::into(self))
    }
}

impl std::str::FromStr for BuildTarget {
    type Err = Error;

    fn from_str(value: &str) -> Result<BuildTarget, Self::Err> {
        use self::BuildTarget::*;
        match value.to_lowercase().as_str() {
            "android" => Ok(Android),
            "ios" => Ok(Ios),
            "n3ds" => Ok(N3ds),
            "ps4" => Ok(Ps4),
            "psp2" => Ok(Psp2),
            "standalonelinux" => Ok(StandaloneLinux),
            "standalonelinux64" => Ok(StandaloneLinux64),
            "standalonelinuxuniversal" => Ok(StandaloneLinuxUniversal),
            "standaloneosx" => Ok(StandaloneOSX),
            "standaloneosxuniversal" => Ok(StandaloneOSXUniversal),
            "standalonewindows" => Ok(StandaloneWindows),
            "standalonewindows64" => Ok(StandaloneWindows64),
            "switch" => Ok(Switch),
            "tvos" => Ok(Tvos),
            "webgl" => Ok(WebGl),
            "wsaplayer" => Ok(WsaPlayer),
            "XboxOne" => Ok(XboxOne),
            _ => Err(Error::ParseBuildTargetError(value.to_string())),
        }
    }
}

impl Into<&'static str> for BuildTarget {
    fn into(self) -> &'static str {
        (&self).into()
    }
}

impl Into<&'static str> for &BuildTarget {
    fn into(self) -> &'static str {
        use self::BuildTarget::*;

        match self {
            Android => "Android",
            Ios => "iOS",
            N3ds => "N3DS",
            Ps4 => "PS4",
            Psp2 => "PSP2",
            StandaloneLinux => "StandaloneLinux",
            StandaloneLinux64 => "StandaloneLinux64",
            StandaloneLinuxUniversal => "StandaloneLinuxUniversal",
            StandaloneOSX => "StandaloneOSX",
            StandaloneOSXUniversal => "StandaloneOSXUniversal",
            StandaloneWindows => "StandaloneWindows",
            StandaloneWindows64 => "StandaloneWindows64",
            Switch => "Switch",
            Tvos => "tvOS",
            WebGl => "WebGL",
            WsaPlayer => "WSAPlayer",
            XboxOne => "XboxOne",
        }
    }
}

impl Serialize for BuildTarget {
    fn serialize<S>(&self, serializer: S) -> Result<S::Ok, S::Error>
    where
        S: Serializer,
    {
        use self::BuildTarget::*;

        match self {
            Android => serializer.serialize_unit_variant("BuildTarget", 0, "Android"),
            Ios => serializer.serialize_unit_variant("BuildTarget", 1, "iOS"),
            N3ds => serializer.serialize_unit_variant("BuildTarget", 2, "N3DS"),
            Ps4 => serializer.serialize_unit_variant("BuildTarget", 3, "PS4"),
            Psp2 => serializer.serialize_unit_variant("BuildTarget", 4, "PSP2"),
            StandaloneLinux => {
                serializer.serialize_unit_variant("BuildTarget", 5, "StandaloneLinux")
            }
            StandaloneLinux64 => {
                serializer.serialize_unit_variant("BuildTarget", 6, "StandaloneLinux64")
            }
            StandaloneLinuxUniversal => {
                serializer.serialize_unit_variant("BuildTarget", 7, "StandaloneLinuxUniversal")
            }
            StandaloneOSX => serializer.serialize_unit_variant("BuildTarget", 8, "StandaloneOSX"),
            StandaloneOSXUniversal => {
                serializer.serialize_unit_variant("BuildTarget", 8, "StandaloneOSXUniversal")
            }
            StandaloneWindows => {
                serializer.serialize_unit_variant("BuildTarget", 9, "StandaloneWindows")
            }
            StandaloneWindows64 => {
                serializer.serialize_unit_variant("BuildTarget", 10, "StandaloneWindows64")
            }
            Switch => serializer.serialize_unit_variant("BuildTarget", 11, "Switch"),
            Tvos => serializer.serialize_unit_variant("BuildTarget", 12, "tvOS"),
            WebGl => serializer.serialize_unit_variant("BuildTarget", 13, "WebGL"),
            WsaPlayer => serializer.serialize_unit_variant("BuildTarget", 14, "WSAPlayer"),
            XboxOne => serializer.serialize_unit_variant("BuildTarget", 15, "XboxOne"),
        }
    }
}

impl<'de> Deserialize<'de> for BuildTarget {
    fn deserialize<D>(deserializer: D) -> Result<BuildTarget, D::Error>
    where
        D: Deserializer<'de>,
    {
        deserializer.deserialize_str(BuildTargetVisitor)
    }
}

struct BuildTargetVisitor;

impl<'de> Visitor<'de> for BuildTargetVisitor {
    type Value = BuildTarget;

    fn expecting(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        f.write_str("A string that indicates a build target, as defined by Unity")
    }

    fn visit_u8<E>(self, value: u8) -> Result<Self::Value, E>
    where
        E: de::Error,
    {
        self.visit_u64(value as u64)
    }

    fn visit_i8<E>(self, value: i8) -> Result<Self::Value, E>
    where
        E: de::Error,
    {
        self.visit_i64(value as i64)
    }

    fn visit_u16<E>(self, value: u16) -> Result<Self::Value, E>
    where
        E: de::Error,
    {
        self.visit_u64(value as u64)
    }

    fn visit_i16<E>(self, value: i16) -> Result<Self::Value, E>
    where
        E: de::Error,
    {
        self.visit_i64(value as i64)
    }

    fn visit_u32<E>(self, value: u32) -> Result<Self::Value, E>
    where
        E: de::Error,
    {
        self.visit_u64(value as u64)
    }

    fn visit_i32<E>(self, value: i32) -> Result<Self::Value, E>
    where
        E: de::Error,
    {
        self.visit_i64(value as i64)
    }

    fn visit_u64<E>(self, value: u64) -> Result<Self::Value, E>
    where
        E: de::Error,
    {
        match value {
            0 => Ok(BuildTarget::Android),
            1 => Ok(BuildTarget::Ios),
            2 => Ok(BuildTarget::N3ds),
            3 => Ok(BuildTarget::Ps4),
            4 => Ok(BuildTarget::Psp2),
            5 => Ok(BuildTarget::StandaloneLinux),
            6 => Ok(BuildTarget::StandaloneLinux64),
            7 => Ok(BuildTarget::StandaloneLinuxUniversal),
            8 => Ok(BuildTarget::StandaloneOSX),
            9 => Ok(BuildTarget::StandaloneWindows),
            10 => Ok(BuildTarget::StandaloneWindows64),
            11 => Ok(BuildTarget::Switch),
            12 => Ok(BuildTarget::Tvos),
            13 => Ok(BuildTarget::WebGl),
            14 => Ok(BuildTarget::WsaPlayer),
            15 => Ok(BuildTarget::XboxOne),
            16 => Ok(BuildTarget::StandaloneOSXUniversal),
            _ => Err(E::custom(format!(
                "unsigned integer out of range: {}",
                value
            ))),
        }
    }

    fn visit_i64<E>(self, value: i64) -> Result<Self::Value, E>
    where
        E: de::Error,
    {
        match value {
            0 => Ok(BuildTarget::Android),
            1 => Ok(BuildTarget::Ios),
            2 => Ok(BuildTarget::N3ds),
            3 => Ok(BuildTarget::Ps4),
            4 => Ok(BuildTarget::Psp2),
            5 => Ok(BuildTarget::StandaloneLinux),
            6 => Ok(BuildTarget::StandaloneLinux64),
            7 => Ok(BuildTarget::StandaloneLinuxUniversal),
            8 => Ok(BuildTarget::StandaloneOSX),
            9 => Ok(BuildTarget::StandaloneWindows),
            10 => Ok(BuildTarget::StandaloneWindows64),
            11 => Ok(BuildTarget::Switch),
            12 => Ok(BuildTarget::Tvos),
            13 => Ok(BuildTarget::WebGl),
            14 => Ok(BuildTarget::WsaPlayer),
            15 => Ok(BuildTarget::XboxOne),
            16 => Ok(BuildTarget::StandaloneOSXUniversal),
            _ => Err(E::custom(format!("signed integer out of range: {}", value))),
        }
    }

    fn visit_str<E>(self, value: &str) -> Result<Self::Value, E>
    where
        E: de::Error,
    {
        BuildTarget::from_str(value).map_err(|e| E::custom(format!("{}", e)))
    }
}

#[derive(Debug, Error)]
pub enum Error {
    #[error("The value {0} is not understood as a variant of the enum BuildTarget")]
    ParseBuildTargetError(String),
}
